import React, {PropTypes} from 'react';
import List from './list';
import Form from './formulario';
import db from './db';
import Feedback from './feedback';

export default React.createClass({

  getInitialState() {
    return {
      formDoc: null,
      cardapio: {pratos: []},
      feedbackMsg: "",
      hasChanges: false
    }
  },

  componentDidMount() {
    db.get('cardapio').then((doc) => {
      this.setState({cardapio: doc});
    });
  },

  render() {
    return <div>
      <Feedback msg={this.state.feedbackMsg}/>

      <List
        data={this.state.cardapio.pratos}
        handleClick={this.handleClick}
      />

      {this.state.formDoc === null
        ? null
        : <Form doc={this.state.formDoc} handleChange={this.handleChange}/>
      }

      {this.state.hasChanges
        ? <div className="action">
          <button onClick={this.saveChanges}>Save</button>
        </div>
        : null
      }

    </div>
  },

  handleClick(item) {
    this.setState({formDoc: item});
    console.log(item)
  },

  handleChange(key, value) {
    const {formDoc} = this.state
    formDoc[key] = value;
    this.setState({formDoc, hasChanges: true});
  },

  saveChanges() {
    db.put(this.state.cardapio).then((result) => {
      const {cardapio} = this.state;
      cardapio._rev = result.rev;
      this.setState({
        cardapio,
        hasChanges: false,
        formDoc: null,
        feedbackMsg: "Dados atualizados com sucesso"
      });
      setTimeout(() => {
        this.setState({feedbackMsg: ''});
      }, 3000);
    });
  }


});
