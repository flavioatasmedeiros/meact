import React, {PropTypes} from 'react';

export default React.createClass({

  propTypes: {
    msg: PropTypes.string.isRequired
  },

  render() {
    return <div className="feedback-wrapper">
      {this.props.msg !== ''
        ? <div className="feedback">
          {this.props.msg}
        </div>
        : null
      }
    </div>
  }

});
