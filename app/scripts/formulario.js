import React, {PropTypes} from 'react';

export default React.createClass({

  propTypes: {
    doc: PropTypes.object.isRequired,
    handleChange: PropTypes.func.isRequired
  },

  render() {
    return <div className="form">
      <input
        type="text"
        value={this.props.doc.nome}
        onChange={this.handleNomeChange}
      />
      <input
        type="text"
        value={this.props.doc.preco}
        onChange={this.handlePrecoChange}
      />
      <input
        type="checkbox"
        checked={this.props.doc.vegetariano === 1 ? "checked" : null}
        onChange={this.handleVeget}
      />
    </div>
  },

  handleNomeChange(ev) {
    const {value} = ev.currentTarget;
    this.props.handleChange('nome', value);
  },

  handlePrecoChange(ev) {
    const {value} = ev.currentTarget;
    this.props.handleChange('preco', parseInt(value, 10));
  },

  handleVeget(ev) {
    const {checked} = ev.currentTarget;
    this.props.handleChange('vegetariano', checked ? 1 : 0);
  }


});
