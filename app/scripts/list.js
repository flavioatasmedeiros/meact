import React, {PropTypes} from 'react';

export default React.createClass({

  propTypes: {
    data: PropTypes.array.isRequired,
    handleClick: PropTypes.func.isRequired
  },

  render() {
    return <ul className="list">
      {this.props.data.map((item) => {
        return <li key={item.id}>
          <a onClick={this.handleClick.bind(null, item)} href="#">
            {item.nome}
          </a>
        </li>
      })}
    </ul>
  },

  handleClick(item, ev) {
    ev.preventDefault();
    this.props.handleClick(item);
  }

});
